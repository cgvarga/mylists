import Vue from 'vue';
import Vuetify from 'vuetify';
import {shallowMount, Wrapper } from '@vue/test-utils'
import MyLists from '@/views/MyLists.vue'

Vue.use(Vuetify);

const defaultData = [
    { id: 1, title: 'List 1', tasks: [] },
    { id: 2, title: 'List 2', tasks: [] },
    { id: 3, title: 'List 3', tasks: [] }
]

describe('MyLists.vue', () => {
    let wrapper: Wrapper<Vue>;
    beforeEach(() => {
        Vue.use(Vuetify);
        localStorage.setItem('lists', JSON.stringify(defaultData));
        wrapper = shallowMount(MyLists, {
            stubs: {
                AddListDialog: {name: 'AddListDialog', template: '<div class="add-list-dialog" />'},
                DeleteListDialog: {name: 'DeleteListDialog', template: '<div class="delete-list-dialog"/>'}
            }
        });
    })
    it('renders 3 list items when data is retreived from local storage', async () => {
        await Vue.nextTick();
        expect(wrapper.findAll('v-list-item-stub')).toHaveLength(3);
    })
    it ('should not render Add List and Delete List Dialogs by default', () => {
        expect(wrapper.vm.$data.adding).toBe(false);
        expect(wrapper.vm.$data.deleting).toBe(false);
    });
    it ('should render an AddList Dialog when Add List Button is clicked', async () => {
        const addButton = wrapper.find('#add-list-btn');
        addButton.vm.$emit('click');
        expect(wrapper.vm.$data.adding).toBe(true);
    })
    it('should render a new List Item when onAdd is called', async () => {
        wrapper.vm.$data.adding = true;
        expect(wrapper.vm.$data.adding).toBe(true);
        await wrapper.vm.$nextTick();
        const dialog = wrapper.find('.add-list-dialog');
        expect(dialog).toBeTruthy();
        dialog.vm.$emit('onAdd', 'New Title');
        await wrapper.vm.$nextTick();
        const listItems = wrapper.findAll('v-list-item-stub');
        expect(listItems.length).toBe(4);
        expect(listItems.at(listItems.length - 1).text()).toContain('New Title');
    });
    it('should show a delete dialog when a delete button is clicked', async () => {
        const listItems = wrapper.findAll('v-list-item-stub');
        const lastItem = listItems.at(listItems.length - 1);
        lastItem.find('[title="Delete List"]').vm.$emit('click');
        expect(wrapper.vm.$data.deleting).toBe(true);
        await wrapper.vm.$nextTick();
        const deleteListDialog = wrapper.findComponent({name:'DeleteListDialog'});
        expect(deleteListDialog.vm).toBeDefined();
    });

    it('should delete a list when a delete is confirmed', async () => {
        expect(wrapper.findAll('v-list-item-stub')).toHaveLength(3);
        const listItems = wrapper.findAll('v-list-item-stub');
        const lastItem = listItems.at(listItems.length - 1);
        lastItem.find('[title="Delete List"]').vm.$emit('click');
        await wrapper.vm.$nextTick();
        const deleteListDialog = wrapper.findComponent({ name: 'DeleteListDialog' });
        deleteListDialog.vm.$emit('onDelete', wrapper.vm.$data.selectedList.id);
        await wrapper.vm.$nextTick();
        expect(wrapper.findAll('v-list-item-stub')).toHaveLength(2);
    });
})
