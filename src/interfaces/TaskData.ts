export default interface TaskData {
    id: string,
    title: string;
    complete: boolean;
}
