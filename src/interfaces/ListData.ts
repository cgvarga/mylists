import TaskData from "./TaskData";

export default interface ListData {
    id: string;
    title: string;
    tasks: TaskData[];
}