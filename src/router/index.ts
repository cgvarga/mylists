import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import MyLists from '../views/MyLists.vue';
import MyTasks from '../views/MyTasks.vue';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'MyLists',
    component: MyLists
  },
  {
    path: '/mylists/:id',
    name: 'MyTasks',
    component: MyTasks,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
